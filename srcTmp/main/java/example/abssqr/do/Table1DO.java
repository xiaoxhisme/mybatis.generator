package example.abssqr.do;


/**
 * The table TABLE1
 */
public class Table1DO{

    /**
     * id ID.
     */
    private Integer id;
    /**
     * card CARD.
     */
    private String card;

    /**
     * Set id ID.
     */
    public void setId(Integer id){
        this.id = id;
    }

    /**
     * Get id ID.
     *
     * @return the string
     */
    public Integer getId(){
        return id;
    }

    /**
     * Set card CARD.
     */
    public void setCard(String card){
        this.card = card;
    }

    /**
     * Get card CARD.
     *
     * @return the string
     */
    public String getCard(){
        return card;
    }
}
